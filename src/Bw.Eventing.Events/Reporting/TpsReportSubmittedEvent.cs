﻿using Bw.Eventing.Framework;
using Bw.Eventing.Framework.Common;

using System;

namespace Bw.Eventing.Events.Reporting
{
    public class TpsReportSubmittedEvent : DomainEventBase
    {
        public TpsReportSubmittedEvent(Guid correlationId, Guid causationId, EventingMethod eventingMethod)
            : base(correlationId, causationId, eventingMethod) { }

        public Guid ReportId { get; set; }
        public Guid SubmittedByUserId { get; set; }
        public string Coversheet { get; set; }
        public string ReportText { get; set; }
    }
}