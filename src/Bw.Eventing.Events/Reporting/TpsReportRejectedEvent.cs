﻿using Bw.Eventing.Framework;
using Bw.Eventing.Framework.Common;

using System;

namespace Bw.Eventing.Events.Reporting
{
    public class TpsReportRejectedEvent : DomainEventBase
    {
        public TpsReportRejectedEvent(Guid correlationId, Guid causationId, EventingMethod eventingMethod)
            : base(correlationId, causationId, eventingMethod) { }
    }
}