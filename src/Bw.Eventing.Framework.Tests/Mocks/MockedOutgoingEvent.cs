﻿using Bw.Eventing.Framework.Common;

using System;

namespace Bw.Eventing.Framework.Tests.Mocks
{
    public class MockedOutgoingEvent : DomainEventBase
    {
        public MockedOutgoingEvent(Guid correlationId, Guid causationId, EventingMethod eventingMethod)
            : base(correlationId, causationId, eventingMethod) { }

        public Guid ReportId { get; set; }
        public string Coversheet { get; set; }
        public string ReportText { get; set; }
        public DateTime CreatedUtc { get; set; }
        public Guid UserId { get; set; }
    }
}