﻿using Microsoft.EntityFrameworkCore;

using System.Threading;
using System.Threading.Tasks;

namespace Bw.Eventing.Framework.Tests
{
    public class EventingDataContext : DbContext, IEventingDbContext
    {
        public EventingDataContext(DbContextOptions<EventingDataContext> options)
            : base(options) { }
        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }
        public new async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}