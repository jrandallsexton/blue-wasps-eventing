﻿using Microsoft.EntityFrameworkCore;

using System;

namespace Bw.Eventing.Framework.Tests
{
    public abstract class UnitTestBase
    {
        public EventingDataContext GetDataContext()
        {
            var dbName = Guid.NewGuid().ToString().Substring(0, 5);
            var options = new DbContextOptionsBuilder<EventingDataContext>()
                .UseInMemoryDatabase(dbName)
                .Options;
            return new EventingDataContext(options);
        }
    }
}