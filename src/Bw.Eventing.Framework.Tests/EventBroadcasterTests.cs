﻿using Bw.Eventing.Framework.Common;
using Bw.Eventing.Framework.Infrastructure;
using Bw.Eventing.Framework.Tests.Mocks;

using FluentAssertions;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Moq;
using Moq.AutoMock;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Xunit;

namespace Bw.Eventing.Framework.Tests
{
    public class EventBroadcasterTests : UnitTestBase
    {
        [Fact]
        public async Task EventBroadcaster_BroadcastsOutgoingEvents()
        {
            var mocker = new AutoMocker();

            // Arrange
            await using var context = base.GetDataContext();
            mocker.Use(typeof(EventingDataContext), context);

            var inMemorySettings = new Dictionary<string, string> {
                {"AzureServiceBus", @"Endpoint=sb://tmcpsb.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=+Lh9UJTQOBt1mpl0dYE3cOIy5gPh8wA2Z3o0Gb9Lo6A="},
                {"AzureQueueName", "test-queue"}
            };
            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();
            mocker.Use(typeof(IConfiguration), configuration);

            var eventBus = new AzureEventBus(configuration);

            /* IServiceProvider */
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider
                .Setup(x => x.GetService(typeof(EventingDataContext)))
                .Returns(context);
            serviceProvider
                .Setup(x => x.GetService(typeof(IProvideEventBus)))
                .Returns(eventBus);

            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(x => x.ServiceProvider).Returns(serviceProvider.Object);

            var serviceScopeFactory = new Mock<IServiceScopeFactory>();
            serviceScopeFactory
                .Setup(x => x.CreateScope())
                .Returns(serviceScope.Object);

            serviceProvider
                .Setup(x => x.GetService(typeof(IServiceScopeFactory)))
                .Returns(serviceScopeFactory.Object);
            /*end IServiceProvider*/

            var mockedEvent = new MockedOutgoingEvent(Guid.NewGuid(), Guid.NewGuid(), EventingMethod.Created);
            await context.OutgoingEvents.AddAsync(new OutgoingEvent()
            {
                Id = Guid.NewGuid(),
                CausationId = Guid.NewGuid(),
                CorrelationId = Guid.NewGuid(),
                EventType = nameof(MockedOutgoingEvent),
                EventData = mockedEvent.ToJson()
            });
            await context.SaveChangesAsync();

            var logger = mocker.GetMock<ILogger<EventBroadcaster<EventingDataContext>>>();

            var broadcaster = new EventBroadcaster<EventingDataContext>(logger.Object, serviceScopeFactory.Object)
                {IsSingleRun = true};

            // Act
            await broadcaster.ExecuteAsync(CancellationToken.None);

            // Assert
            var outgoing = await context.OutgoingEvents.FirstAsync();
            outgoing.BroadcastUtc.Should().HaveValue("because the event should have been sent to the bus");
        }
    }
}