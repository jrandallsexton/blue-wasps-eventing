﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bw.Eventing.Framework.Infrastructure
{
    public interface IProvideEventBus
    {
        Task SendAsync(OutgoingEvent outgoingEvent);
        Task SendRangeAsync(List<OutgoingEvent> outgoingEvents);
        Task<List<IncomingEvent>> ReceiveAsync();
    }
}