﻿using Azure.Messaging.ServiceBus;

using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bw.Eventing.Framework.Infrastructure
{
    public class AzureEventBus : IProvideEventBus
    {
        private readonly string _connectionString;
        private readonly string _queueName;

        public AzureEventBus(IConfiguration config)
        {
            _connectionString = config.GetValue<string>("AzureServiceBus");
            _queueName = config.GetValue<string>("AzureQueueName");
        }

        public async Task SendAsync(OutgoingEvent outgoingEvent)
        {
            var sbClient = new ServiceBusClient(_connectionString);
            var sender = sbClient.CreateSender(_queueName);
            var messageBody = JsonSerializer.Serialize(outgoingEvent);
            var message = new ServiceBusMessage(Encoding.UTF8.GetBytes(messageBody));
            try
            {
                await sender.SendMessageAsync(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("foo");
                throw;
            }
            
        }

        public async Task SendRangeAsync(List<OutgoingEvent> outgoingEvents)
        {

            if (outgoingEvents.Count == 1)
            {
                await SendAsync(outgoingEvents.First());
                return;
            }
                
            var sbClient = new ServiceBusClient(_connectionString);
            var sender = sbClient.CreateSender(_queueName);

            try
            {
                using ServiceBusMessageBatch messageBatch = await sender.CreateMessageBatchAsync();

                foreach (var e in outgoingEvents)
                {
                    var messageBody = JsonSerializer.Serialize(e);
                    var message = new ServiceBusMessage(Encoding.UTF8.GetBytes(messageBody));
                    messageBatch.TryAddMessage(message);
                }
                await sender.SendMessagesAsync(messageBatch);
            }
            catch (Exception ex)
            {
                Console.WriteLine("foo");
            }

        }

        public Task<List<IncomingEvent>> ReceiveAsync()
        {
            throw new NotImplementedException();
        }
    }
}