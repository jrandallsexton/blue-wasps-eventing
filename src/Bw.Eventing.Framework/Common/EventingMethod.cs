﻿namespace Bw.Eventing.Framework.Common
{
    public enum EventingMethod
    {
        Created = 1,
        Updated = 2,
        Deleted = 3
    }
}