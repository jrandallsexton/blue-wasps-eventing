﻿using Newtonsoft.Json;

namespace Bw.Eventing.Framework.Common
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object obj, JsonSerializerSettings settings = null)
        {
            settings ??= new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            return JsonConvert.SerializeObject(obj, Formatting.None, settings);
        }
    }
}