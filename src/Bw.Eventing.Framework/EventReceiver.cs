﻿using Microsoft.Extensions.Logging;

namespace Bw.Eventing.Framework
{
    public class EventReceiver
    {
        private ILogger<EventReceiver> _logger;

        public EventReceiver(ILogger<EventReceiver> logger)
        {
            _logger = logger;
        }
    }
}