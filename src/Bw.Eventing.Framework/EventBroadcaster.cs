﻿using Bw.Eventing.Framework.Infrastructure;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bw.Eventing.Framework
{
    public interface IBroadcastEvents
    {
        Task ExecuteAsync(CancellationToken cancellationToken);
    }

    public class EventBroadcaster<TDbContext> : IBroadcastEvents where TDbContext : IEventingDbContext
    {
        private readonly ILogger<EventBroadcaster<TDbContext>> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private TDbContext _context;
        private IProvideEventBus _eventBus;
        public bool IsSingleRun { get; set; }
        private bool _hasRanOnce;

        public EventBroadcaster(ILogger<EventBroadcaster<TDbContext>> logger, IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested && (IsSingleRun && !_hasRanOnce))
            {
                using var scope = _scopeFactory.CreateScope();

                _context = scope.ServiceProvider.GetService<TDbContext>();
                _eventBus = scope.ServiceProvider.GetService<IProvideEventBus>();

                _hasRanOnce = true;

                // open connection to the event queue

                // grab messages to be sent
                var outgoingEvents = await _context.OutgoingEvents
                    .Where(e => !e.BroadcastUtc.HasValue)
                    .ToListAsync(cancellationToken);

                if (!outgoingEvents.Any()) continue;
                {
                    // send them
                    try
                    {
                        await _eventBus.SendRangeAsync(outgoingEvents);

                        // mark as sent
                        outgoingEvents.ForEach(e => e.BroadcastUtc = DateTime.UtcNow);

                        await _context.SaveChangesAsync(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "some shit happened");
                    }
                }
            }
        }
    }
}