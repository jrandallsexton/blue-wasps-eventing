﻿using System;

namespace Bw.Eventing.Framework
{
    public class OutgoingEvent
    {
        public Guid Id { get; set; }
        public Guid CorrelationId { get; set; }
        public Guid CausationId { get; set; }
        public string EventType { get; set; }
        public string EventData { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime? BroadcastUtc { get; set; }
    }
}