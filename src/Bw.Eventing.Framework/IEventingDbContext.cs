﻿using Microsoft.EntityFrameworkCore;

using System.Threading;
using System.Threading.Tasks;

namespace Bw.Eventing.Framework
{
    public interface IEventingDbContext
    {
        public DbSet<OutgoingEvent> OutgoingEvents { get; set; }
        public DbSet<IncomingEvent> IncomingEvents { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}