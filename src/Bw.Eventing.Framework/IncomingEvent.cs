﻿using System;

namespace Bw.Eventing.Framework
{
    public class IncomingEvent
    {
        public Guid Id { get; set; }
        public Guid CorrelationId { get; set; }
        public Guid CausationId { get; set; }
        public string EventType { get; set; }
        public string EventData { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime? ProcessedUtc { get; set; }
    }
}