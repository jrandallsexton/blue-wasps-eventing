﻿using Bw.Eventing.Framework.Common;

using System;

namespace Bw.Eventing.Framework
{
    public abstract class DomainEventBase
    {
        protected DomainEventBase(Guid correlationId, Guid causationId, EventingMethod eventingMethod)
        {
            CorrelationId = correlationId;
            CausationId = causationId;
            EventMethod = eventingMethod;
        }

        public Guid CorrelationId { get; set; }

        public Guid CausationId { get; set; }

        public EventingMethod EventMethod { get; set; }

        public DateTime CreatedUtc { get; set; } = DateTime.UtcNow;

        public Guid CreatedBy { get; set; }

        public string CreatedByName { get; set; }
    }
}