﻿using Microsoft.Extensions.Hosting;

using System.Threading;
using System.Threading.Tasks;

namespace Bw.Eventing.Framework
{
    public class EventBroadcasterHost<T>: BackgroundService where T : IEventingDbContext
    {
        private readonly IBroadcastEvents _broadcaster;

        public EventBroadcasterHost(IBroadcastEvents broadcaster)
        {
            _broadcaster = broadcaster;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await _broadcaster.ExecuteAsync(cancellationToken);
            }
        }
    }
}